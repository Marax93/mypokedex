import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListPkmComponent } from './components/list-pkm/list-pkm.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DetailsPkmComponent } from './components/details-pkm/details-pkm.component';
import { BtnMyTeamComponent } from './components/btn-my-team/btn-my-team.component';
import { MyTeamComponent } from './components/my-team/my-team.component';

@NgModule({
  declarations: [
    AppComponent,
    ListPkmComponent,
    NavbarComponent,
    DetailsPkmComponent,
    BtnMyTeamComponent,
    MyTeamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
