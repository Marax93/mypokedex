import { MyTeamComponent } from './components/my-team/my-team.component';
import { DetailsPkmComponent } from './components/details-pkm/details-pkm.component';
import { ListPkmComponent } from './components/list-pkm/list-pkm.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:  '' , component: ListPkmComponent },
  { path:  'detailsPkm/:pkmId' , component: DetailsPkmComponent },
  { path:  'myTeam' , component: MyTeamComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
