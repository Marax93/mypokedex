import { PokemonService } from './../../service/pokemon.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details-pkm',
  templateUrl: './details-pkm.component.html',
  styleUrls: ['./details-pkm.component.css']
})
export class DetailsPkmComponent {

  pkmIdFromRoute!: number;
  pkm: any;
  pokemon: any[] = this.pokemonService.getPokemonList();


  constructor(private route: ActivatedRoute, private pokemonService: PokemonService) {
    const routeParams = this.route.snapshot.paramMap; // mappa il link della pagina
    this.pkmIdFromRoute = Number(routeParams.get('pkmId'));
  }

  ngOnInit() {
    this.pkm = this.pokemonService.getPokemonById(this.pkmIdFromRoute); // il pkm è preso dall'id che gli arriva, usa il metodo del servizio

  }


  addFavorites(pkm: any) {
    this.pokemonService.addToFavorites(pkm);
  }


}
