import { PokemonService } from './../../service/pokemon.service';
import { Component } from '@angular/core';


@Component({
  selector: 'app-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.css']
})
export class MyTeamComponent {


  favorites: any[] = [];


  constructor( private pokemonService: PokemonService) {


  }

  ngOnInit() {
    this.favorites = this.pokemonService.getFavorites();
  }


  removeFavorites(pkm: any){
    this.pokemonService.removeFavorites(pkm);
    this.favorites = this.pokemonService.getFavorites();
  }


 

}
