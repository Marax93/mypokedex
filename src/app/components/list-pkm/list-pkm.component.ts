import { PokemonService } from './../../service/pokemon.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-pkm',
  templateUrl: './list-pkm.component.html',
  styleUrls: ['./list-pkm.component.css'],
})
export class ListPkmComponent implements OnInit{


  constructor(private pokemonService: PokemonService) {} // passo i dati dei pokemon dal servizio

  pokemonList: any[] = this.pokemonService.getPokemonList();
  filteredPokemonList?: any[];

  searchText: string = '';




  ngOnInit(): void {
    this.pokemonList; // se il campo di ricerca è vuoto mi appare la lista originale
    this.filterPokemonList();
  }


  filterPokemonList() {
    this.filteredPokemonList = this.pokemonList.filter(pokemon => {
      // filtro per nome
      if (this.searchText && !pokemon.name.toLowerCase().includes(this.searchText.toLowerCase())) {
        return false; // se quello che scrivo E il nome del pokemon NON (!) include il testo scritto, allora escludilo dalla lista filtrata
      }
      return true;
    });
  }


//   filterPokemonList() è una funzione che viene eseguita per filtrare una lista di pokemon.

// this.filteredPokemonList è un array che conterrà la lista filtrata di pokemon.

// this.pokemonList è l'array di partenza che contiene tutti i pokemon.

// this.pokemonList.filter() è un metodo degli array di JavaScript che restituisce un nuovo array contenente solo gli elementi che rispettano la condizione specificata.

// pokemon => {...} è una funzione callback che viene eseguita per ogni elemento dell'array this.pokemonList. La variabile pokemon rappresenta ogni singolo pokemon dell'array.

// if (this.searchText && !pokemon.name.toLowerCase().includes(this.searchText.toLowerCase())) è una condizione che controlla se il testo
// di ricerca è presente nel nome del pokemon.
// La proprietà name rappresenta il nome del pokemon. toLowerCase() è un metodo che converte la stringa in minuscolo per effettuare la comparazione
// in modo case-insensitive.
// Se il testo di ricerca non è presente, la funzione filter() restituisce false per escludere il pokemon dall'array filtrato.

// return true; viene eseguito se la condizione del punto 6 non si verifica e il pokemon viene incluso nell'array filtrato.

// Infine, la lista filtrata viene assegnata alla proprietà filteredPokemonList e la funzione termina.



}
